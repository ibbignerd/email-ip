This is a quick script I wrote for my Raspberry Pi which runs on startup. It gets the IP address from the wlan0 interface (can be changed to eth0) and sends it via gmail account to another email address. 

**Please note**: You must make the gmail account less secure via [settings](https://www.google.com/settings/security/lesssecureapps)

You can also use email text messages. http://www.emailtextmessages.com/