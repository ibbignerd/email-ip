import smtplib
import socket
import fcntl
import struct
import creds #Local file that contains personal info

#Globals
emailUsr = "" #sender email user name
emailAddr = "" #sender email address
emailPwd = "" #sender email password
emailRcpt = "" #recipient email address

emailUsr = creds.emailUsr
emailAddr = creds.emailAddr
emailPwd = creds.emailPwd
emailRcpt = creds.emailRcpt


def main():
    IP = get_ip_address("wlan0")
    print("IP:" + IP)

    emailDoc(IP)
    print("exit 0")

#DOC: return IP address based on ifname input
def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(s.fileno(),0x8915,
        struct.pack('256s', ifname[:15]))[20:24])

#DOC: email given IP based on creds
def emailDoc(givenIP):
    header = 'From: %s\n' % emailAddr
    header += 'To: %s\n\n' % emailRcpt
    message = header + givenIP

    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.starttls()
    print "Logging into account"
    server.login(emailUsr, emailPwd)
    returnStatement = server.sendmail(emailAddr, emailRcpt, message)
    server.quit

main()
